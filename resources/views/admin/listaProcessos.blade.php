@extends('layouts.app')
@section('styles')
<link rel="stylesheet" href="{{ asset('libs/datatables/datatables.bootstrap4.min.css') }}">
@endsection
@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading clearfix"> 
			<span id="titulo-painel" class="h3-semlinha"> Filas de Processamento </span>
		</div>
		<div class="panel-body">
			<div class="row table-responsive">  
				<div class="col-md-12"> 
					<table class="table table-bordered" id="table" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Processo</th>
								<th>Status</th>
								<th>Log</th>
								<th>Ano</th>
								<th>Trimestre</th>
								<th>Cidade</th>
								<th>Usuario</th>
								<th>Começou em</th>
								<th>Última atualização</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libs/datatables/datatables.min.js') }}"></script>
<script>
	$(function() {
		var table =  $('#table').DataTable({
			"language": {
				"url": "{{ asset('plugins/datatables.net/js/Portuguese-Brasil.json')}}"
			},
			processing: true,
			serverSide: true,
			ajax: {
				type: 'POST',
				dataType: "json",
				url: '{!! route('filaProcessamento') !!}',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			columns: [
			{ data: 'id', name: 'id'},
			{ data: 'Processo', name: 'Processo'},
			{ data: 'Log', name: 'Log'},
			{ data: 'Status', name: 'Status',
			render: function ( data, type, row, meta ) {
				if(data == 3){
					return 'Erro';
				}else if(data == 2){
					return 'Cancelado';
				}else if(data == 1){
					return 'Finalizado';
				}else if(data == 0){
					return 'Iniciado';
				}
			},
		}, 
		{ data: 'Ano', name: 'Ano'},
		{ data: 'Trimestre', name: 'Trimestre'},
		{ data: 'cidade.municipio', name: 'cidade.municipio'},
		{ data: 'user.nome', name: 'user.nome'},
		{ data: 'created_at', name: 'processos.created_at'},
		{ data: 'updated_at', name: 'processos.updated_at'},
		],
		"order": [[ 6, "desc" ]]
	});

	});
</script>
@endsection