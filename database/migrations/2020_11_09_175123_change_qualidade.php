<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeQualidade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qualidade', function (Blueprint $table) {
            $table->string('NCOMISSAO', 255)->change();
            $table->string('COMISSAOGD', 255)->change();
            $table->string('COMISSAOFORM', 255)->change();
            $table->string('COMISSAODOC', 255)->change();
            $table->string('DTCOMISSAO', 255)->change();
            $table->string('NCOMISSAO', 255)->change();
            $table->string('UPDECRETOCOMISSAO', 255)->change();
            $table->string('MAPEAMENTO', 255)->change();
            $table->string('LIMPEZA', 255)->change();
            $table->string('LISTAUNICA', 255)->change();
            $table->string('FATORRISCO', 255)->change();
            $table->string('INDICADOROBITO', 255)->change();
            $table->string('PRILINKAGE', 255)->change();
            $table->string('ULTLINKAGE', 255)->change();
            $table->string('COMOFOILISTAVITIMAS')->change();
            $table->string('NAOLINKOBITO', 255)->change();
            $table->string('NAOLINKFER', 255)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
