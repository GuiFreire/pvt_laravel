<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePlano extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plano_acoes', function (Blueprint $table) {
            $table->string('MetaIntermediaria', 255)->change();
            $table->string('MetaFinal', 255)->change();
            $table->string('ParceriasPublicas', 255)->change();
            $table->string('ParceriasPrivadas', 255)->change();
            $table->string('ParceriasCivil', 255)->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
